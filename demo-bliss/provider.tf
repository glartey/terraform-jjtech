terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.44.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.28.1"
    }

    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.98.0"
    }
  }

}


# provider "aws" {
# #  profile = "default"
# #  region = "us-east-1"
# }

provider "aws" {
  shared_config_files      = ["/Users/Anselme/.aws/config"]
  shared_credentials_files = ["/Users/Anselme/.aws/credentials"]
  profile                  = "ddefault"
  region = "ca-central-1"
}



