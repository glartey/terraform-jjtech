
terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket = "innovative20244"
    key    = "innovative/terraform.tfstate"
    region = "us-east-1"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "lock-table"
  }
}